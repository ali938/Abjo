package coleo.com.abjo.service;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;

import coleo.com.abjo.constants.Constants;

public class PedometerHandler implements SensorEventListener {

    private boolean firstTime = true;

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        long count = (long) sensorEvent.values[0];
        if (firstTime) {
            Constants.getRepository().updateFirstStep(count);
            firstTime = false;
        } else {
            Constants.lastUpdateCount = count;
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}
