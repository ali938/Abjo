package coleo.com.abjo.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import coleo.com.abjo.R;

public class SendJsonDialog extends Dialog {

    private float mainCount;
    private float done;
    private TextView mainTextView;

    public SendJsonDialog(@NonNull final Context context, int mainCount) {
        super(context);
        setContentView(R.layout.upload_json_layout);

        this.mainCount = mainCount;

        mainTextView = findViewById(R.id.description_upload_text_view_id);
        mainTextView.setText(context.getString(R.string.sending_information_text) + "\n" + "0%");
        ImageView watch = findViewById(R.id.sand_watch_image_view_id);
        watch.setBackgroundResource(R.drawable.sand_watch_animation);
        AnimationDrawable watchAnimationDrawable = (AnimationDrawable) watch.getBackground();
        watchAnimationDrawable.start();

        ImageView watchBack = findViewById(R.id.sand_watch_back_image_view_id);
        RotateAnimation anim = new RotateAnimation(0f, 359f
                , Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        anim.setInterpolator(new LinearInterpolator());
        anim.setRepeatCount(Animation.INFINITE);
        anim.setDuration(2000);
        watchBack.startAnimation(anim);

        View view = findViewById(R.id.dialog_layout);
        view.setOnClickListener(v -> {

        });
    }

    public void addDonePercent() {
        done++;
        float donePercent = (done / mainCount) * 100;
        String temp = String.format("%.2f", donePercent);
        mainTextView.setText(getContext().getString(R.string.sending_information_text) + "\n" + temp + " % ");
    }


}
