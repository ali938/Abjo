package coleo.com.abjo.data_class;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class ProfileData implements Serializable {

    private User user;
    private int coins;
    private int hours;
    private String note;
    private String introduceCode;
    private UserLevel level;

    public ProfileData(User user, int coins, int hours, UserLevel level, String note, String introduceCode) {
        this.user = user;
        this.coins = coins;
        this.hours = hours;
        this.level = level;
        this.introduceCode = introduceCode;
        setNote(note);
    }

    public static ProfileData maker(HashSet<String> set) {
        if (set == null)
            return null;
        int coin = 0, hours = 0;
        String first = null, last = null, number = null, note = null;
        String introduceCode = null;
        int level = 0, point = 0, total = 0, max = 0, rank = 0;
        for (String temp : set) {
            String start = temp.substring(0, 2);
            temp = temp.substring(2);
            switch (start) {
                case "1.": {
                    first = temp;
                    break;
                }
                case "2.": {
                    last = temp;
                    break;
                }
                case "3.": {
                    number = temp;
                    break;
                }
                case "4.": {
                    coin = Integer.parseInt(temp);
                    break;
                }
                case "5.": {
                    hours = Integer.parseInt(temp);
                    break;
                }
                case "6.": {
                    note = temp;
                    break;
                }
                case "7.": {
                    level = Integer.parseInt(temp);
                    break;
                }
                case "8.": {
                    point = Integer.parseInt(temp);
                    break;
                }
                case "9.": {
                    total = Integer.parseInt(temp);
                    break;
                }
                case "0.": {
                    max = Integer.parseInt(temp);
                    break;
                }
                case "a.": {
                    rank = Integer.parseInt(temp);
                    break;
                }
                case "b.": {
                    introduceCode = temp;
                    break;
                }
            }
        }
        User user = new User(first, last, number, false);
        UserLevel userLevel = new UserLevel(level, point, total, max, rank);
        return new ProfileData(user, coin, hours, userLevel, note, introduceCode);
    }

    public String getIntroduceCode() {
        return introduceCode;
    }

    public void setNote(String note) {
        if (note == null) {
            this.note = " ";
        } else {
            if (note.isEmpty()) {
                this.note = " ";
            } else
                this.note = note;
        }
    }

    public String getNote() {
        return note;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setCoins(int coins) {
        this.coins = coins;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public void setLevel(UserLevel level) {
        this.level = level;
    }

    public User getUser() {
        return user;
    }

    public int getCoins() {
        return coins;
    }

    public String getCoinsText() {
        return " " + coins + " سکه ";
    }

    public String getHoursText() {
        return " " + hours + " ساعت ";
    }

    public int getHours() {
        return hours;
    }

    public UserLevel getLevel() {
        return level;
    }

    public void setIntroduceCode(String introduceCode) {
        this.introduceCode = introduceCode;
    }

    public Set<String> toSet() {
        HashSet<String> list = new HashSet<>(user.toSet());
        list.add("4." + coins);
        list.add("5." + hours);
        list.add("6." + note);
        list.addAll(level.toSet());
        list.add("b." + introduceCode);
        return list;
    }

}
