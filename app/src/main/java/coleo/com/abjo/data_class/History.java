
package coleo.com.abjo.data_class;

import coleo.com.abjo.constants.Constants;

public class History extends DateAction {

    private int coin;
    private int point;
    private int distance;
    private String imageUrl;

    public History(DateAction dateAction, int coin, int point, int distance, String imageUrl) {
        super(dateAction);
        this.coin = coin;
        this.point = point;
        this.distance = distance;
        setImageUrl(imageUrl);
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = Constants.Base_Url_media + imageUrl;
    }

    public void setCoin(int coin) {
        this.coin = coin;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public int getCoin() {
        return coin;
    }

    public int getPoint() {
        return point;
    }

    public int getDistance() {
        return distance;
    }

}
