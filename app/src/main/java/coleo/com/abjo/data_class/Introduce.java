package coleo.com.abjo.data_class;

import coleo.com.abjo.constants.Constants;

public class Introduce extends DateAction {

    private int coin;
    private String imageUrl;
    private String title;

    public Introduce(DateAction dateAction, int coin, String title, String imageUrl) {
        super(dateAction);
        this.coin = coin;
        this.title = title;
        setImageUrl(imageUrl);
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = Constants.Base_Url_media + imageUrl;
    }

    public void setCoin(int coin) {
        this.coin = coin;
    }

    public int getCoin() {
        return coin;
    }

    public String getTitle() {
        return title;
    }
}
