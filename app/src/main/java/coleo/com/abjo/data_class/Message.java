package coleo.com.abjo.data_class;

public class Message {

    private String text;
    private String title;
    private boolean seen;

    public Message(String text, String title, boolean seen) {
        this.text = text;
        this.title = title;
        this.seen = seen;
    }

    public boolean isSeen() {
        return seen;
    }

    public void setSeen(boolean seen) {
        this.seen = seen;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public String getTitle() {
        return title;
    }
}
