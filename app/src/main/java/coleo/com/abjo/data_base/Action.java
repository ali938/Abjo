package coleo.com.abjo.data_base;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Action {

    @ColumnInfo(name = "time")
    public long time;

    @PrimaryKey(autoGenerate = true)
    public int number;

    @ColumnInfo(name = "session_id")
    public long id;

    public Action(long id, String action, long step) {
        this.id = id;
        this.time = System.currentTimeMillis();
        this.action = action;
        this.step = step;
        synced = false;
    }

    @ColumnInfo(name = "action_type")
    public String action;

    @ColumnInfo(name = "step_count")
    public long step;

    @ColumnInfo(name = "synced")
    public boolean synced;

}
