package coleo.com.abjo.data_base;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface PauseDao {

    @Query("SELECT * FROM `Action` WHERE session_id=:section AND action_type='start'")
    List<Action> getAll(long section);

    @Query("DELETE FROM `Action` WHERE synced = :sync")
    void nukeTable(boolean sync);

    @Query("SELECT * FROM `Action` WHERE synced = :sync")
    List<Action> getNotSynced(boolean sync);

    @Query("UPDATE `Action` SET synced=:fa WHERE number IN (:input)")
    void sync(boolean fa, int[] input);

    @Query("UPDATE `Action` SET step_count=:count WHERE session_id=:section AND action_type='start'")
    void firstStepUpdate(long count, long section);

    @Insert
    void insertAll(Action... users);

    @Delete
    void delete(Action user);

}
