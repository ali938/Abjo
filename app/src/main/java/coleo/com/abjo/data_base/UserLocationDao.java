package coleo.com.abjo.data_base;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface UserLocationDao {
    @Query("SELECT * FROM UserLocation")
    List<UserLocation> getAll();

    @Query("SELECT * FROM UserLocation WHERE synced=:input ORDER BY time")
    List<UserLocation> getNotSynced(boolean input);

    @Query("DELETE FROM UserLocation WHERE synced= :input")
    void nukeTable(boolean input);

    @Query("UPDATE `UserLocation` SET synced= :fa WHERE number IN (:input)")
    void sync(boolean fa, int[] input);

    @Insert
    void insertAll(UserLocation... users);

    @Delete
    void delete(UserLocation user);

}
