package coleo.com.abjo.data_base;

import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.util.Pair;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import coleo.com.abjo.activity.MainActivity;
import coleo.com.abjo.constants.Constants;
import coleo.com.abjo.server_class.ServerClass;
import coleo.com.abjo.service.SaverReceiver;

import static coleo.com.abjo.constants.Constants.context;

public class locationRepository {

    private UserLocationDao userLocationDao;
    private PauseDao pauseDao;
    private TravelDataBase travelDataBase;
    private static locationRepository repository;

    public void close() {
//        travelDataBase.close();
    }

    public void setUserLocationDao(UserLocationDao userLocationDao) {
        this.userLocationDao = userLocationDao;
    }

    public void setActionDao(PauseDao userLocationDao) {
        this.pauseDao = userLocationDao;
    }

    private locationRepository(TravelDataBase travelDataBase) {
        this.travelDataBase = travelDataBase;
        this.userLocationDao = travelDataBase.userDao();
    }

    public static locationRepository get(TravelDataBase travelDataBase) {
        if (repository != null) {
            return repository;
        } else {
            if (travelDataBase != null) {
                repository = new locationRepository(travelDataBase);
                return repository;
            }
            return null;
        }
    }

    public void insert(UserLocation location) {
        new insertAsyncTask(userLocationDao).execute(location);
    }

    public void updateFirstStep(long step) {
        Long[] temp = new Long[2];
        temp[0] = step;
        temp[1] = Constants.getSession();
        Log.i("LOCATION_REPO", "updateFirstStep: " + step + "  sec = " + temp[1]);
        new UpdateFirstStep(pauseDao).execute(temp);
    }

    public void insert(Action action) {
        new insertPauseAsyncTask(pauseDao).execute(action);
    }

    public void nukeTable() {
        new nukePauseAsyncTask(pauseDao).execute();
        new nukeAsyncTask(userLocationDao).execute();
    }

    public void makeJsonAndSend() {
        new getAsyncTask(userLocationDao, pauseDao).execute();
    }

    public void updateUnsynced(MainActivity activity) {
        CountAsyncTask temp = new CountAsyncTask(pauseDao, userLocationDao);
        temp.execute(activity);
    }

    public void sync(JSONObject data, boolean action) {
        if (action) {
            Integer[] numbers = getActionNumber(data);
            SyncActionsAsyncTask temp = new SyncActionsAsyncTask(pauseDao);
            temp.execute(numbers);
        } else {
            Integer[] numbers = getLocationNumber(data);
            SyncLocationAsyncTask temp = new SyncLocationAsyncTask(userLocationDao);
            temp.execute(numbers);
        }
    }

    private Integer[] getActionNumber(JSONObject data) {
        ArrayList<Integer> numbers = new ArrayList<>();
        try {
            JSONArray array = data.getJSONArray("actions");
            for (int i = 0; i < array.length(); i++)
                numbers.add(((JSONObject) array.get(i)).getInt("id"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Integer[] out = new Integer[numbers.size()];
        for (int i = 0; i < out.length; i++) {
            out[i] = numbers.get(i);
        }
        return out;
    }

    private Integer[] getLocationNumber(JSONObject data) {
        ArrayList<Integer> numbers = new ArrayList<>();
        try {
            JSONArray array = data.getJSONArray("locations");
            for (int i = 0; i < array.length(); i++)
                numbers.add(((JSONObject) array.get(i)).getInt("id"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Integer[] out = new Integer[numbers.size()];
        for (int i = 0; i < out.length; i++) {
            out[i] = numbers.get(i);
        }
        return out;
    }

    private static class insertPauseAsyncTask extends AsyncTask<Action, Void, Void> {

        private PauseDao mAsyncTaskDao;

        insertPauseAsyncTask(PauseDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Action... params) {
            mAsyncTaskDao.insertAll(params[0]);
            return null;
        }
    }

    private static class UpdateFirstStep extends AsyncTask<Long[], Void, Void> {

        private PauseDao mAsyncTaskDao;

        UpdateFirstStep(PauseDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Long[]... arrays) {
//            int temp = mAsyncTaskDao.getAll(arrays[0][1]).size();
//            Log.i("WHAT TO UP", "doInBackground: " + temp);
            mAsyncTaskDao.firstStepUpdate(arrays[0][0], arrays[0][1]);
            return null;
        }
    }

    private static class insertAsyncTask extends AsyncTask<UserLocation, Void, Void> {

        private UserLocationDao mAsyncTaskDao;

        insertAsyncTask(UserLocationDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final UserLocation... params) {
            UserLocation temp = params[0];
            for (int i = 0; i < 3; i++) {
                mAsyncTaskDao.insertAll(temp);
            }
            return null;
        }
    }

    private static class nukeAsyncTask extends AsyncTask<Void, Void, Void> {

        private UserLocationDao mAsyncTaskDao;

        nukeAsyncTask(UserLocationDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            mAsyncTaskDao.nukeTable(true);
            return null;
        }
    }

    private static class SyncLocationAsyncTask extends AsyncTask<Integer[], Void, Void> {

        private UserLocationDao mAsyncTaskDao;

        SyncLocationAsyncTask(UserLocationDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Integer[]... arrays) {
            Integer[] temp = arrays[0];
            int[] input = new int[temp.length];
            for (int i = 0; i < temp.length; i++) {
                input[i] = temp[i];
            }
            mAsyncTaskDao.sync(true, input);
            return null;
        }
    }

    private static class SyncActionsAsyncTask extends AsyncTask<Integer[], Void, Void> {

        private PauseDao mAsyncTaskDao;

        SyncActionsAsyncTask(PauseDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Integer[]... arrays) {
            Integer[] temp = arrays[0];
            int[] input = new int[temp.length];
            for (int i = 0; i < temp.length; i++) {
                input[i] = temp[i];
            }
            mAsyncTaskDao.sync(true, input);
            return null;
        }
    }

    private static class nukePauseAsyncTask extends AsyncTask<Void, Void, Void> {

        private PauseDao mAsyncTaskDao;

        nukePauseAsyncTask(PauseDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            mAsyncTaskDao.nukeTable(true);
            return null;
        }
    }

    private static class getAsyncTask extends AsyncTask<Void, Void, ArrayList<Pair<JSONObject, Integer>>> {

        private UserLocationDao mAsyncTaskDao;
        private PauseDao pauseDao;

        getAsyncTask(UserLocationDao dao, PauseDao pauseDao) {
            this.pauseDao = pauseDao;
            mAsyncTaskDao = dao;
        }

        @Override
        protected ArrayList<Pair<JSONObject, Integer>> doInBackground(Void... voids) {
            return makeJson(mAsyncTaskDao.getNotSynced(false), pauseDao.getNotSynced(false));
        }

        int MAX_IN_JSON = 50;

        @Override
        protected void onPostExecute(ArrayList<Pair<JSONObject, Integer>> jsonObject) {
            super.onPostExecute(jsonObject);
            Intent intent = new Intent(context, SaverReceiver.class);
            context.stopService(intent);
            if (ServerClass.isNetworkConnected(context))
                ServerClass.sendActivityData(jsonObject, context, 0, null);
            else {
                ((MainActivity) context).restart();
            }
        }

        private ArrayList<Pair<JSONObject, Integer>> makeJson(List<UserLocation> locations, List<Action> actions) {
            ArrayList<Pair<JSONObject, Integer>> output = new ArrayList<>();
            try {
                ArrayList<Pair<ArrayList<UserLocation>, Integer>> locationsBySession = new ArrayList<>();
                main:
                for (UserLocation temp : locations) {
                    for (Pair<ArrayList<UserLocation>, Integer> p : locationsBySession) {
                        if (p.second == temp.id) {
                            p.first.add(temp);
                            continue main;
                        }
                    }
                    Pair<ArrayList<UserLocation>, Integer> pair = new Pair<>(new ArrayList<>(), (int) temp.id);
                    pair.first.add(temp);
                    locationsBySession.add(pair);
                }

                ArrayList<Pair<ArrayList<Action>, Integer>> actionsBySession = new ArrayList<>();
                main:
                for (Action temp : actions) {
                    for (Pair<ArrayList<Action>, Integer> p : actionsBySession) {
                        if (p.second == temp.id) {
                            p.first.add(temp);
                            continue main;
                        }
                    }
                    Pair<ArrayList<Action>, Integer> pair = new Pair<>(new ArrayList<>(), (int) temp.id);
                    pair.first.add(temp);
                    actionsBySession.add(pair);
                }


                for (Pair<ArrayList<UserLocation>, Integer> pair : locationsBySession) {
                    ArrayList<Action> pauseOfThisSession = new ArrayList<>();
                    for (Pair<ArrayList<Action>, Integer> tempActionsPair : actionsBySession) {
                        if (tempActionsPair.second.equals(pair.second)) {
                            for (Action action : tempActionsPair.first) {
                                if (action.action.equals("pause")) {
                                    pauseOfThisSession.add(action);
                                }
                            }
                        }
                    }


                    if (pauseOfThisSession.isEmpty()) {
                        locationToJson(pair.first, pair.second, output);
                    } else {
                        ArrayList<ArrayList<UserLocation>> locationsByPause = new ArrayList<>();
                        int locationCount = 0;
                        pause:
                        for (Action pause : pauseOfThisSession) {
                            ArrayList<UserLocation> thisPause = new ArrayList<>();
                            for (; locationCount < pair.first.size(); locationCount++) {
                                UserLocation location = pair.first.get(locationCount);
                                if (location.time < pause.time) {
                                    thisPause.add(location);
                                } else {
                                    locationsByPause.add(thisPause);
                                    continue pause;
                                }
                            }
                        }
                        ArrayList<UserLocation> afterLastPause = new ArrayList<>();
                        for (; locationCount < pair.first.size(); locationCount++) {
                            afterLastPause.add(pair.first.get(locationCount));
                        }
                        locationsByPause.add(afterLastPause);
                        for (ArrayList<UserLocation> pausePart : locationsByPause) {
                            locationToJson(pausePart, pair.second, output);
                        }
                    }

                }

                for (Pair<ArrayList<Action>, Integer> pair : actionsBySession) {
                    int i = 0;
                    JSONArray tempArray = new JSONArray();
                    for (Action p : pair.first) {
                        JSONObject temp = new JSONObject();
                        temp.put("id", p.number);
                        temp.put("datetime", p.time);
                        temp.put("kind", p.action);
                        temp.put("step_count", p.step);
                        i++;
                        tempArray.put(temp);
                        if (i == MAX_IN_JSON) {
                            i = 0;
                            JSONObject realOne = new JSONObject();
                            realOne.put("actions", tempArray);
                            output.add(new Pair<>(realOne, pair.second));
                            tempArray = new JSONArray();
                        }
                    }
                    if (tempArray.length() > 0) {
                        JSONObject realOne = new JSONObject();
                        realOne.put("actions", tempArray);
                        output.add(new Pair<>(realOne, pair.second));
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }


            return output;
        }

        private void locationToJson(ArrayList<UserLocation> list, Integer session, ArrayList<Pair<JSONObject, Integer>> output) {
            JSONArray tempArray = new JSONArray();
            int i = 0;
            for (UserLocation location : list) {
                JSONObject locationJsonObject = new JSONObject();
                try {
                    locationJsonObject.put("id", location.number);
                    locationJsonObject.put("latitude", location.latitude);
                    locationJsonObject.put("longitude", location.longitude);
                    locationJsonObject.put("accuracy", location.accuracy);
                    locationJsonObject.put("datetime", location.time);
                    tempArray.put(locationJsonObject);
                    i++;
                    if (i == MAX_IN_JSON) {
                        i = 0;
                        JSONObject realOne = new JSONObject();
                        realOne.put("locations", tempArray);
                        output.add(new Pair<>(realOne, session));
                        tempArray = new JSONArray();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            try {
                if (tempArray.length() > 0) {
                    JSONObject realOne = new JSONObject();
                    realOne.put("locations", tempArray);
                    output.add(new Pair<>(realOne, session));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }


    private static class CountAsyncTask extends AsyncTask<MainActivity, Void, Integer> {

        private PauseDao pauseDao;
        private UserLocationDao userLocationDao;

        CountAsyncTask(PauseDao pauseDao, UserLocationDao userLocationDao) {
            this.pauseDao = pauseDao;
            this.userLocationDao = userLocationDao;
        }

        @Override
        protected Integer doInBackground(MainActivity... mainActivities) {
            List<Action> actions = pauseDao.getNotSynced(false);
            List<UserLocation> locations = userLocationDao.getNotSynced(false);
            ArrayList<Long> seen = new ArrayList<>();
            for (Action action : actions) {
                if (!action.synced) {
                    if (!seen.contains(action.id)) {
                        seen.add(action.id);
                    }
                }
            }
            for (UserLocation location : locations) {
                if (!location.synced) {
                    if (!seen.contains(location.id)) {
                        seen.add(location.id);
                    }
                }
            }
            mainActivities[0].runOnUiThread(() -> mainActivities[0].syncCount(seen.size()));
            return null;
        }
    }

}
