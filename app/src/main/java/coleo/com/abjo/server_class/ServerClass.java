package coleo.com.abjo.server_class;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Pair;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import coleo.com.abjo.activity.MainActivity;
import coleo.com.abjo.activity.login.CodeActivity;
import coleo.com.abjo.activity.login.Login;
import coleo.com.abjo.activity.login.SignUpActivity;
import coleo.com.abjo.activity.login.Splash;
import coleo.com.abjo.activity.menu.MassageActivity;
import coleo.com.abjo.constants.Constants;
import coleo.com.abjo.data_class.DateAction;
import coleo.com.abjo.data_class.History;
import coleo.com.abjo.data_class.Introduce;
import coleo.com.abjo.data_class.LeaderBoardData;
import coleo.com.abjo.data_class.Message;
import coleo.com.abjo.data_class.NewUserForServer;
import coleo.com.abjo.data_class.ProfileData;
import coleo.com.abjo.data_class.Transition;
import coleo.com.abjo.data_class.User;
import coleo.com.abjo.data_class.UserLevel;
import coleo.com.abjo.dialogs.SendJsonDialog;

import static coleo.com.abjo.constants.Constants.getErrorMessage;

public class ServerClass {

    private static final String TAG = "server class";

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        assert cm != null;
        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }

        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();
    }

    private static void saveToken(Context context, JSONObject request) {
        try {
            Constants.setToken(context, request.getString("token"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void handleError(Context context, VolleyError error) {
        error.printStackTrace();
        if (error.networkResponse == null) {
            Toast.makeText(context, "اتصال اینترنت خود را بررسی کنید", Toast.LENGTH_SHORT).show();
        } else {
            if (error.networkResponse.statusCode == 403) {
                Toast.makeText(context, "کلید شما منقضی شده", Toast.LENGTH_SHORT).show();
                Constants.setToken(context, "");
                Intent intent = new Intent(context, Splash.class);
                context.startActivity(intent);
                ((Activity) context).finish();
                return;
            }
            if (error.networkResponse.statusCode != 417) {
                error.printStackTrace();
                Toast.makeText(context, getErrorMessage(error), Toast.LENGTH_LONG).show();
            } else {
                error.printStackTrace();
                Toast.makeText(context, getErrorMessage(error), Toast.LENGTH_SHORT).show();
            }
        }
    }

    public static void appStartup(final Context context, boolean fromNotification) {

        String url = Constants.URL_START_APP;

        JSONObject temp = new JSONObject();
        try {
            temp.put("app_version", Constants.VERSION);
            temp.put("android_version", "" + Build.VERSION.SDK_INT);
//            Log.i("find ok version", "appStartup: " + Build.VERSION.SDK_INT);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ObjectRequest jsonObjectRequest = new ObjectRequest
                (context, Request.Method.POST, url, temp,
                        response -> {
                            saveToken(context, response);
                            ((Splash) context).goMain();
                        },
                        error -> {
                            if (error == null) {
                                Splash activity = (Splash) context;
                                if (Constants.isActiveSession()) {
                                    Intent intent = new Intent(context, MainActivity.class);
                                    intent.putExtra(Constants.FROM_NOTIFICATION, fromNotification);
                                    activity.startActivity(intent);
                                    activity.finish();
                                } else {
                                    activity.enable();
                                    Toast.makeText(context, "اتصال اینترنت خود را بررسی کنید", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                handleError(context, error);
                            }
                        }
                );

        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);

    }

    public static void checkPhone(final Context context, final String phone) {

        String url = Constants.URL_CHECK_PHONE;
        url += phone;
        url += "/";

        FirstObjectRequest jsonObjectRequest = new FirstObjectRequest
                (context, Request.Method.GET, url, null, response ->
                        ((Login) context).goCode(phone),
                        error -> {
                            ((Login) context).enable();
                            handleError(context, error);
                        });

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                (int) TimeUnit.SECONDS.toMillis(20),
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }

    public static void sendCode(final Context context, final String phone, final String code) {

        String url = Constants.URL_SEND_CODE;
        url += phone;
        url += "/";
        url += code;
        url += "/";

        StrImplRequest request = new StrImplRequest(Request.Method.GET, url, response -> {
            JSONObject object = null;
            try {
                object = new JSONObject(response);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            saveToken(context, object);
            if (StrImplRequest.isNewUser) {
                ((CodeActivity) context).goSignUp();
                ((CodeActivity) context).finish();
            } else {
                ((CodeActivity) context).goMainPage();
            }
        }, error -> {
            ((CodeActivity) context).wrongCode();
            ServerClass.handleError(context, error);

        });
        request.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getInstance(context).addToRequestQueue(request);

    }

    public static void getProfile(final Context context) {

        String url = Constants.URL_GET_USER_PROFILE;

        ObjectRequest jsonObjectRequest = new ObjectRequest
                (context, Request.Method.GET, url, null,
                        response -> {
                            saveToken(context, response);
//                                Log.i(TAG, "onResponse: getProfile : ");
                            ((MainActivity) context).updateProfile(parseProfile(response));
                        }
                        , error -> {
                    if (error != null) {
                        if (error.networkResponse != null) {
                            if (error.networkResponse.statusCode == 201) {
                                String jsonString = new String(error.networkResponse.data);
                                try {
                                    saveToken(context, new JSONObject(jsonString));
                                    ((CodeActivity) context).goSignUp();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                    ServerClass.handleError(context, error);
                }
                );

        MySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);

    }

    public static void makeNewUser(final Context context, final NewUserForServer user) {

        String url = Constants.URL_MAKE_USER;

        JSONObject mainJson = new JSONObject();
        JSONObject userJsonObject = new JSONObject();
        try {
            userJsonObject.put("first_name", user.getFirstName());
            userJsonObject.put("last_name", user.getLastName());
            userJsonObject.put("student_id", user.getStudentId());
            userJsonObject.put("is_woman", user.isWoman());
            userJsonObject.put("introduce_by", user.getIntroduceCode());
            userJsonObject.put("app_version", Constants.VERSION);
            userJsonObject.put("android_version", "" + Build.VERSION.SDK_INT);
            mainJson.put("user", userJsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ObjectRequest jsonObjectRequest = new ObjectRequest
                (context, Request.Method.POST, url, mainJson,
                        response -> {
                            saveToken(context, response);
                            ((SignUpActivity) context).goMain();
                        }
                        , error -> {
                    ((SignUpActivity) context).enable();
                    ServerClass.handleError(context, error);
                }
                );

        MySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);

    }

    private static ProfileData parseProfile(JSONObject response) {
//        Log.i(TAG, "parseProfile: " + response.toString());
        int coins, hours;
        String note = " ";
        String introduceCode = " ";
        try {
            User user = parseUser(response);
            try {
                note = response.getString("note");
            } catch (JSONException e) {
//                Log.i(TAG, "parseProfile: note not found");
            }
            JSONObject user_data = response.getJSONObject("user_data");
            introduceCode = user_data.getString("introduce_code");
            coins = user_data.getInt("coins");
            hours = user_data.getInt("hours");
            UserLevel level = parseLevel(user_data);
            return new ProfileData(user, coins, hours, level, note, introduceCode);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void getLeaderBoard(final Context context) {
        String url = Constants.URL_GET_LEADER_BOARD;

        ObjectRequest jsonObjectRequest = new ObjectRequest
                (context, Request.Method.GET, url, null,
                        response -> {
//                                Log.i(TAG, "getLeaderBoard: " + response.toString());
                            saveToken(context, response);
                            ArrayList<LeaderBoardData> arrayList = new ArrayList<>();
                            try {
                                JSONArray users = response.getJSONArray("users");
                                for (int i = 0; i < users.length(); i++) {
                                    LeaderBoardData temp = parseLeaderBoardUser(users.getJSONObject(i));
                                    if (temp != null)
                                        arrayList.add(temp);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            ((MainActivity) context).updateLeaderBoard(arrayList);
                        }
                        , error -> ServerClass.handleError(context, error)
                );

        MySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }

    public static void getHistory(final Context context, int page) {
        String url = Constants.URL_GET_HISTORY;
        url += page;
        url += "/";
        ((MainActivity) context).hideHistory();

        ObjectRequest jsonObjectRequest = new ObjectRequest
                (context, Request.Method.GET, url, null,
                        response -> {
//                                Log.i(TAG, "get history: " + response.toString());
                            saveToken(context, response);
                            ArrayList<DateAction> actions = new ArrayList<>();
                            try {
                                JSONArray records = response.getJSONArray("user_records");
                                for (int i = 0; i < records.length(); i++) {
                                    DateAction temp = historyParser(records.getJSONObject(i));
                                    if (temp != null)
                                        actions.add(temp);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            ((MainActivity) context).updateHistory(actions);
                        }
                        , error -> {
                    ServerClass.handleError(context, error);
                    ((MainActivity) context).showHistory();
                }
                );

        MySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }

    private static DateAction historyParser(JSONObject object) {
        DateAction date = DateParser(object);
        try {
            assert date != null;
            date.setId(object.getInt("id"));
            String kind = object.getString("type");
            int coin = object.getInt("coin");
            String imageUrl = object.getJSONObject("icon").getString("url");
            switch (kind) {
                case "spend": {
                    String title = object.getString("text");
                    return new Transition(date, title, coin, "");
                }
                case "introduce": {
                    String title = object.getString("text");
                    return new Introduce(date, coin, title, imageUrl);
                }
                case "activity": {
                    int point = object.getInt("point");
                    int distance = object.getInt("distance");
                    return new History(date, coin, point, distance, imageUrl);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static DateAction DateParser(JSONObject object) {
        try {
            JSONObject date = object.getJSONObject("date_created");
            String dayName = "", monthName = "";
            try {
                dayName = date.getString("day_name");
                monthName = date.getString("month_name");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return new DateAction(date.getInt("day"), date.getInt("month"), date.getInt("year"),
                    date.getInt("hour"), date.getInt("minute"), date.getInt("second"), dayName, monthName
            );
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static LeaderBoardData parseLeaderBoardUser(JSONObject raw) {
        try {
            String first = raw.getString("first_name");
            String last = raw.getString("last_name");
            int point = raw.getInt("point");
            int rank = raw.getInt("rank");
            boolean is_mine = raw.getBoolean("is_you");
            User user = new User(first, last, "", false);
            return new LeaderBoardData(user, rank, point, is_mine, false);//todo blur
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static User parseUser(JSONObject response) {
//        Log.i(TAG, "parseUser: " + response.toString());
        String first, last, number;
        try {
            JSONObject user = response.getJSONObject("user_data");
            first = user.getString("first_name");
            last = user.getString("last_name");
            number = user.getString("phone");
            return new User(first, last, number, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static UserLevel parseLevel(JSONObject response) {
        UserLevel level = new UserLevel();
//        Log.i(TAG, "parseLevel: " + response);
        try {
            JSONObject data = response.getJSONObject("user_level");
            level.setPoint(data.getInt("user_point"));
            JSONObject user_level = data.getJSONObject("user_level");
            level.setLevelMaxPoint(user_level.getInt("capacity"));
            level.setLevelImageUrl(user_level.getJSONObject("logo").getString("url"));
            level.setLevel(user_level.getInt("number"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return level;
    }

    public static void openNewSession(final Context context, final boolean step, final ProfileData data) {
        String url = Constants.URL_OPEN_SESSION;

        JSONObject mainJson = new JSONObject();
        JSONObject session = new JSONObject();
        try {
            int type = (step) ? 1 : 2;
            mainJson.put("activity_type_id", type);
            mainJson.put("date_start", "" + System.currentTimeMillis());
            session.put("session", mainJson);
//            Log.i(TAG, "onResponse: newSession from mine: " + session.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ObjectRequest jsonObjectRequest = new ObjectRequest
                (context, Request.Method.POST, url, session, response -> {
//                    Log.i(TAG, "onResponse: newSession : " + response.toString());
                    try {
                        JSONObject session1 = response.getJSONObject("session");
                        Constants.setSession(session1.getInt("id"));
                        ((MainActivity) context).enableButton();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Constants.saveUserSession(data);
                    ((MainActivity) context).showAfterStart(step, data);
                }, error -> handleError(context, error));

        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }

    public static void sendActivityData(ArrayList<Pair<JSONObject, Integer>> data, Context context, int id, SendJsonDialog dialog) {

        if (!isNetworkConnected(context)) {
            ((MainActivity) context).restart();
        } else {
            if (id == 0) {
                SendJsonDialog sendJsonDialog = new SendJsonDialog(context, data.size());
                dialog = sendJsonDialog;
                sendJsonDialog.show();
            }
            if (id < data.size() - 1) {
                Pair<JSONObject, Integer> p = data.get(id);
                JSONObject object = p.first;
                if (object.has("actions")) {
                    sendActions(data, context, dialog, id + 1, p.second);
                } else if (object.has("locations")) {
                    sendLocations(data, context, dialog, id + 1, p.second);
                }
            } else {
                Pair<JSONObject, Integer> p = data.get(data.size() - 1);
                JSONObject object = p.first;
                if (object.has("actions")) {
                    sendActions(data, context, dialog, -1, p.second);
                } else if (object.has("locations")) {
                    sendLocations(data, context, dialog, -1, p.second);
                }
            }
        }

    }

    public static void getMessage(final Context context, ArrayList<Message> messages) {
        String url = Constants.URL_GET_MESSAGE;

        ObjectRequest jsonObjectRequest = new ObjectRequest
                (context, Request.Method.GET, url, null,
                        response -> {
                            saveToken(context, response);
                            try {
                                JSONArray records = response.getJSONArray("messages");
                                for (int i = 0; i < records.length(); i++) {
                                    Message temp = parseMessage(records.getJSONObject(i));
                                    if (temp != null)
                                        messages.add(temp);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            ((MassageActivity) context).update();
                        }
                        , error -> {
                    ServerClass.handleError(context, error);
                }
                );

        MySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }

    private static Message parseMessage(JSONObject object) {
        try {
            String title, text;
            boolean seen = object.getBoolean("seen");
            title = object.getString("title");
            text = object.getString("body");
            return new Message(text, title, seen);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static JSONObject makeSmall(JSONObject data) {
        JSONObject out = new JSONObject();
        try {
            String lo;
            double v;
            StringBuilder temp = new StringBuilder();
            JSONArray array = data.getJSONArray("locations");
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);
                lo = ("" + object.getLong("datetime")).substring(0, 13);
//                Log.i(TAG, "makeSmall: lo0 = " + lo);
                temp.append(lo);

                lo = object.getString("longitude");
                v = Double.parseDouble(lo);
                lo = String.format("%2.6f", v);
                while (lo.length() < 9)
                    lo = lo.concat("0");
                lo = lo.replace(".", "");
                temp.append(lo);
//                Log.i(TAG, "makeSmall: lo1 = " + lo);

                lo = object.getString("latitude");
                v = Double.parseDouble(lo);
                lo = String.format("%2.6f", v);
                while (lo.length() < 9)
                    lo = lo.concat("0");
                lo = lo.replace(".", "");
                temp.append(lo);
//                Log.i(TAG, "makeSmall: lo2 = " + lo);

                double aDouble = Double.parseDouble(object.getString("accuracy"));
                StringBuilder acc = new StringBuilder("" + aDouble);
                int index = acc.indexOf(".");
                do {
                    if (index > 3) {
                        acc = new StringBuilder(acc.substring(1));
                    } else if (index < 3) {
                        acc.insert(0, "0");
                    }
                    index = acc.indexOf(".");
                } while (index != 3);
                while (acc.length() < 7) {
                    acc.append("0");
                }
                acc = new StringBuilder(acc.toString().replace(".", ""));
                temp.append(acc);
            }
            out.put("text", temp.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return out;
    }

    private static void sendActions(ArrayList<Pair<JSONObject, Integer>> arrayList, Context context, SendJsonDialog dialog, int id, int session) {

        String url = Constants.URL_OPEN_SESSION + session + Constants.URL_ACTION_SEND;
        JSONObject data;
        if (id == -1) {
            data = arrayList.get(arrayList.size() - 1).first;
        } else
            data = arrayList.get(id - 1).first;
        ObjectRequest jsonObjectRequest = new ObjectRequest
                (context, Request.Method.POST, url, data, response -> {
//                    Log.i(TAG, "onResponse: action send : ");
                    dialog.addDonePercent();
                    Constants.getRepository().sync(data, true);
                    if (id == -1) {
                        Toast.makeText(context, "بروز رسانی تمام شد.", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                        Constants.getNotSendSession(((MainActivity) context));
                    } else {
                        sendActivityData(arrayList, context, id, dialog);
                    }
                }, error -> {
                    handleError(context, error);
                    if (id == -1) {
                        dialog.dismiss();
                    }
//                    MySingleton.getInstance(context).getRequestQueue().stop();
//                    handleError(context, error);
                });

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                (int) TimeUnit.SECONDS.toMillis(20),
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }

    private static void sendLocations(ArrayList<Pair<JSONObject, Integer>> arrayList, Context context, SendJsonDialog dialog, int id, int session) {

        String url = Constants.URL_OPEN_SESSION + session + Constants.URL_LOCATION_SEND_COM;
        JSONObject data;
        if (id == -1) {
            data = arrayList.get(arrayList.size() - 1).first;
        } else
            data = arrayList.get(id - 1).first;
        ObjectRequest jsonObjectRequest = new ObjectRequest
                (context, Request.Method.POST, url, makeSmall(data), response -> {
//                    Log.i(TAG, "onResponse: location send : ");
                    dialog.addDonePercent();
                    Constants.getRepository().sync(data, false);
                    if (id == -1) {
                        Toast.makeText(context, "بروز رسانی تمام شد.", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                        Constants.getNotSendSession(((MainActivity) context));
                    } else {
                        sendActivityData(arrayList, context, id, dialog);
                    }
                }, error -> {
                    handleError(context, error);
                    if (id == -1) {
                        dialog.dismiss();
                    }
//                    ((MainActivity) context).restart();
//                    MySingleton.getInstance(context).getRequestQueue().stop();
//                    handleError(context, error);
                });

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                (int) TimeUnit.SECONDS.toMillis(20),
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }

    public static class StrImplRequest extends StringRequest {

        static boolean isNewUser = false;

        StrImplRequest(int method, String url, Response.Listener<String> listener, @Nullable Response.ErrorListener errorListener) {
            super(method, url, listener, errorListener);
        }

        @Override
        public Map<String, String> getHeaders() {
            HashMap<String, String> headers = new HashMap<>();
            headers.put("Content-Type", "application/json");
            headers.put("role", "user");
            return headers;
        }

        @Override
        protected Response<String> parseNetworkResponse(NetworkResponse response) {
            int temp = response.statusCode;
//            Log.i(TAG, "parseNetworkResponse: ");
            if (temp == 201 || temp == 202) {
                isNewUser = true;
            }
            return super.parseNetworkResponse(response);
        }


    }

}
