package coleo.com.abjo.activity.login;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Objects;

import coleo.com.abjo.R;
import coleo.com.abjo.constants.Constants;
import coleo.com.abjo.server_class.ServerClass;

public class Login extends AppCompatActivity {

    private EditText phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        boolean show = false;
        try {
            Intent intent = getIntent();
            show = Objects.requireNonNull(intent.getExtras()).getBoolean("From_Code", false);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ImageView back = findViewById(R.id.back_login);
        phone = findViewById(R.id.phone_input_id);

//
//        Glide.with(this).load(R.mipmap.temp_pic)
//                .override(back., back.getLayoutParams().height)
//                .into(back);
//        back.getLayoutParams().width = Constants.getScreenWidth(this);
        back.getLayoutParams().height = (int) (Constants.getScreenHeight(this) - Constants.dpToPx(this, 150));
        back.requestLayout();

        if (show) {
            phone.setText(Constants.getLastCheckedPhone());
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case (R.id.submit_login_id): {
                findViewById(R.id.submit_login_id).setEnabled(false);
                if (getPhoneNumber().equals(Constants.getLastCheckedPhone()) &&
                        Constants.getLastTime() + 120000 > System.currentTimeMillis())
                    goCode(getPhoneNumber());
                else
                    ServerClass.checkPhone(this, getPhoneNumber());
            }
        }
    }

    private String getPhoneNumber() {
        return phone.getText().toString().trim();
    }

    public void goCode(String phone) {
        findViewById(R.id.submit_login_id).setEnabled(false);
        Constants.saveLastPhone(System.currentTimeMillis(), phone);
        Intent intent = new Intent(this, CodeActivity.class);
        intent.putExtra(Constants.PHONE_FROM_LOGIN, phone);
        startActivity(intent);
        finish();
    }

    public void enable() {
        findViewById(R.id.submit_login_id).setEnabled(true);
    }

}
